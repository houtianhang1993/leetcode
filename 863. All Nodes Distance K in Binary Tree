/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
// 思路： 1.use HashMap, and BFS
//       2.build a undirected graph using treenode as vertices, using parent-child relation as edges
//       2.do BFS with the source vertices(target) to find all vertices which are K distance to it.
          
class Solution {
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        if(root == null){
            return new ArrayList<Integer>();
        }
        List<Integer> res = new ArrayList<>();
        Map<Integer, List<Integer>> map = new HashMap<>();
        buildGraph(root, null, map);
        
        Set<Integer> isVisited = new HashSet<>();
        Queue<Integer> q = new LinkedList<>();
        q.offer(target.val);
        isVisited.add(target.val);
        
        while(!q.isEmpty()){
            if(K == 0){
                res.addAll(q);
                break;
            }
            
            int size = q.size();    
            for(int i=0; i<size; i++){
                int curNode = q.poll();
                List<Integer> neighbors = map.get(curNode);
                
                for(int j=0; j<neighbors.size(); j++){
                    int next = neighbors.get(j);
                    if(!isVisited.contains(next)){
                        q.offer(next);
                        isVisited.add(next);
                    }
                }
                 
            }
            K--;
        }
        return res;
    }
    
    private void buildGraph(TreeNode cur, TreeNode parent, Map<Integer, List<Integer>> map){
        if(cur == null){
            return;
        }
        map.put(cur.val, new ArrayList<Integer>());
        if(parent != null){
            map.get(cur.val).add(parent.val);
            map.get(parent.val).add(cur.val);
        }
        buildGraph(cur.left, cur, map);
        buildGraph(cur.right, cur, map);
    }
}