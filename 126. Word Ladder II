如果要返回所有的结果，问题变复杂了些。
因为用BFS相对于DFS的劣势就是不方便存储结果。这种需要返回所有结果的，还是应该从DFS考虑
但是直接应用DFS复杂度会很高，因为这道题我们只要知道结尾就好了，不用继续往下搜。

所以问题就转化成怎样用DFS的同时又可以限制DFS的深度，所以我们可以BFS与DFS结合。
先用BFS搜到结尾字符串，然后把中途所有的字符串及其跟起始字符的edit distance存在一个map里。
这样的话，我们就可以从结尾字符串开始DFS，只有Map内的字符串才考虑继续DFS，直至搜到起始字符。

注意这里有个小技巧，就是为什么不从起始字符串开始DFS直至搜到结尾字符串，而是反过来。
这里可以脑子里想像一个图，如果从起始字符串开始搜，到最后一层的话会有很多无效搜索
因为那层我们只需要找到结尾字符串，那么多无效的搜索到最一层太浪费时间。
反之，如果我们从结尾字符串开始DFS, 我们把起始层控制在一个字符串
整个图先越来越宽，然后越来越窄直到起始字符串，而非一直越来越宽直到结尾字符串那层。

time: O(n), space: O(n)

class Solution {
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        if(!wordList.contains(endWord)){
            return new ArrayList<>();
        }
        List<List<String>> res = new ArrayList<>();
        List<String> cur = new ArrayList<>();
        Map<String, Integer> distMap = new HashMap<>();
        bfsGetDistance(beginWord, endWord, wordList, distMap);
        dfs(res, cur, distMap, wordList, endWord, beginWord);
        return res;
    }
    private void dfs(List<List<String>> res, List<String> cur, Map<String, Integer> distMap, List<String> wordList, String word, String des){
        if(word.equals(des)){ //倒叙
            List<String> list = new ArrayList<>(cur);
            list.add(des);
            Collections.reverse(list);
            res.add(list);
            return;
        }
        
        cur.add(word);
        for(int i=0; i<word.length(); i++){
            char[] chars = word.toCharArray();
            for(char letter = 'a'; letter <='z'; letter++){
                chars[i] = letter;
                String nextWord = new String(chars);
                
                // 不仅字典中含有，两字符串也是要在路径的相邻位置即距离差1
                if(distMap.containsKey(nextWord) && distMap.get(nextWord) == distMap.get(word) -1){
                    dfs(res, cur, distMap, wordList, nextWord, des);
                }
            }
        }
        cur.remove(cur.size()-1);
    }
    // 用Word Ladder I的方法把候选字符串及其距离存入map，缩小DFS范围。
    private void bfsGetDistance(String beginWord, String endWord, List<String> wordList, Map<String, Integer> distMap){
        distMap.put(beginWord, 1);
        Queue<String> q = new LinkedList<>();
        q.offer(beginWord);
        
        while(!q.isEmpty()){
            String word = q.poll();
            for(int i=0; i<word.length(); i++){
                char[] chars = word.toCharArray();
                for(char letter = 'a'; letter<='z'; letter++){
                    chars[i] = letter;
                    String nextWord = new String(chars);
                    if(nextWord.equals(endWord)){
                        distMap.put(nextWord, distMap.get(word) + 1);
                        return;
                    }
                    if(wordList.contains(nextWord) && !distMap.containsKey(nextWord)){
                        distMap.put(nextWord, distMap.get(word) + 1);
                        q.offer(nextWord);
                    }
                }
            }
        }
    }
}